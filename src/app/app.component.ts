import {Component, OnInit} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {LangSettings, extractCode, userLanguage} from "./languages";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private translate: TranslateService) {
    translate.addLangs(LangSettings.SUPPORTED.map(lang => extractCode(lang.locale)));
    translate.setDefaultLang(extractCode(LangSettings.DEFAULT));
    translate.use(userLanguage());
  }

  ngOnInit(): void {}

}
