import {Component, OnInit} from '@angular/core';
import {add, differenceInWeeks, format, formatDuration, intervalToDuration, parseISO} from 'date-fns';
import {Settings} from "../settings-component/settings.component";
import {LangSettings, userLanguage} from "../languages";

@Component({
  selector: 'main-component',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  private dateBirthday!: Date;
  private dateAdult!: Date;
  public dateBirthdayString!: string;
  public timeBirthdayString!: string;
  public name!: string;

  dateNow!: Date;

  ageList!: string[];
  adulthood!: string;
  weeks!: string;
  showWeeks!: boolean;

  locale!: Locale;

  constructor() {
  }

  ngOnInit(): void {
    this.applyLocale();

    this.name = localStorage.getItem(Settings.Name) || 'Эллен';
    this.dateBirthday = parseISO(localStorage.getItem(Settings.Birthday) || '2021-04-12 23:03:00');
    this.dateBirthdayString = format(this.dateBirthday, "do MMMM yyyy", {locale: this.locale});
    this.timeBirthdayString = format(this.dateBirthday, "HH:mm");
    this.dateAdult = add(this.dateBirthday, {years: Number.parseInt(localStorage.getItem(Settings.Adulthood) || '18')});
    this.showWeeks = localStorage.getItem(Settings.ShowWeeks) == 'true';

    this.refresh();
    setInterval(() => this.refresh(), 1000)
  }

  applyLocale(): void {
    const language = userLanguage();
    this.locale = LangSettings.SUPPORTED
      .map(l => l.locale)
      .find(l => l.code === language) || LangSettings.DEFAULT;
  }

  refresh(): void {
    this.dateNow = new Date();

    const duration = intervalToDuration({start: this.dateBirthday, end: this.dateNow});
    this.ageList = formatDuration(duration, {locale: this.locale, zero: true, delimiter: '<br>'}).split('<br>')

    const fullWeeks = differenceInWeeks(this.dateNow, this.dateBirthday);
    this.weeks = formatDuration({weeks: fullWeeks}, {format: ['weeks'], locale: this.locale})

    const age = this.dateNow.getTime() - this.dateBirthday.getTime();
    const adult = this.dateAdult.getTime() - this.dateBirthday.getTime();
    this.adulthood = Number(age / adult * 100).toFixed(6);
  }
}
