import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {DateTimeValidator} from "./DateTimeValidator";
import {Router} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";
import {extractCode, LangSettings} from "../languages";

@Component({
  selector: 'app-settings-component',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  readonly supportedLangs = LangSettings.SUPPORTED.map(l => ({
    locale: extractCode(l.locale),
    name: l.name
  }));

  formGroup = new FormGroup({

    name: new FormControl(
      localStorage.getItem(Settings.Name) || 'Эллен',
      Validators.required
    ),

    birthday: new FormControl(
      localStorage.getItem(Settings.Birthday) || "2021-04-12 23:03",
      [
        Validators.required,
        DateTimeValidator.validator
      ]
    ),

    adulthood: new FormControl(Number.parseInt(
      localStorage.getItem(Settings.Adulthood) || '18'),
      [
        Validators.required,
        Validators.min(1)
      ]
    ),

    showWeeks: new FormControl(localStorage.getItem(Settings.ShowWeeks) == 'true'),

    language: new FormControl(this.translate.currentLang),

  })

  constructor(private router: Router, public translate: TranslateService) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    localStorage.setItem(Settings.Name, this.formGroup.value.name)
    localStorage.setItem(Settings.Birthday, this.formGroup.value.birthday)
    localStorage.setItem(Settings.Adulthood, this.formGroup.value.adulthood)
    localStorage.setItem(Settings.ShowWeeks, this.formGroup.value.showWeeks)
    localStorage.setItem(Settings.Language, this.formGroup.value.language)
    this.translate.use(this.formGroup.value.language)
    this.goBack();
  }

  goBack() {
    if (history.length > 1) {
      history.back()
    } else {
      this.router.navigateByUrl('/');
    }
  }
}

export const Settings = {
  Name: 'EllenName',
  Birthday: 'EllenBirthday',
  Adulthood: 'EllenAdulthood',
  ShowWeeks: 'EllenWeeks',
  Language: 'EllenLanguage',
}
