import {FormControl, Validators} from "@angular/forms";
import {isValid, parse} from "date-fns";

export class DateTimeValidator {

  static validator(birthday: FormControl): Validators {
    return isValid(parse(
      birthday.value,
      'yyyy-MM-dd HH:mm',
      new Date())
    )
      ? {}
      : {invalid: true};
  }
}
