import {Settings} from "./settings-component/settings.component";
import {enGB, et, ru} from "date-fns/locale";

export const LangSettings = {
  DEFAULT: enGB,
  SUPPORTED: [
    {locale: enGB, name: 'English'},
    {locale: et, name: 'Eesti'},
    {locale: ru, name: 'Русский'}
  ],
}

export function userLanguage(): string {
  const supported = LangSettings.SUPPORTED.map(lang => extractCode(lang.locale));
  const browserPreference = navigator.language.substr(0, 2);

  const defaultLang = supported.includes(browserPreference)
    ? browserPreference
    : LangSettings.DEFAULT.code!;

  return localStorage.getItem(Settings.Language) || defaultLang;
}

export function extractCode(locale: Locale): string {
  return locale.code!.substr(0, 2); // 'en-US' -> 'en'
}
