# EllenAge

This is a simple progressive web application dedicated to my recently birthed daughter Ellen.  
I made it to practice with Angular, PWA and multi-language support.

The idea is to know the exact baby's age expressed in months/days/hours/minutes/seconds.  
Later my wife asked me to add an option to see baby's age expressed in weeks (added in settings).

Thanks to PWA technology the application is runnnable at any time in any place even without being connected to the Internet.

### Languages

For multi-language support I used [ngx-translate](https://github.com/ngx-translate/core). Very easy and convenient library.  
At the time of writing this, there are English, Estonian and Russian languages added.

## Demo

I deployed the application to bitbucket pages. Check it [here](https://gta2k.bitbucket.io/EllenAge)
